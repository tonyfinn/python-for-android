#!/bin/bash

rm -Rf build/*
rm -Rf dist/auto
./distribute.sh -C -f -m pydroid -d auto
cd dist/auto
./build.py --dir ~/pydroid/samples/MyRent --version 1 --name "Pydroid MyRent" \
--package "com.tonyfinn.pydroid.myrent" --orientation portrait debug
