#!/bin/bash

VERSION_pdpyjnius=${VERSION_pdpyjnius:-master}
URL_pdpyjnius=http://jenkins.tonyfinn.com/builds/pyjnius-pydroid.tar.gz
DEPS_pdpyjnius=(python jniwrap)
MD5_pdpyjnius=
BUILD_pdpyjnius=$BUILD_PATH/pdpyjnius/$(get_directory $URL_pdpyjnius)
RECIPE_pdpyjnius=$RECIPES_PATH/pdpyjnius

function prebuild_pdpyjnius() {
	true
}

function shouldbuild_pdpyjnius() {
	if [ -d "$SITEPACKAGES_PATH/jnius" ]; then
		DO_BUILD=0
	fi
}

function build_pdpyjnius() {
	cd $BUILD_pdpyjnius

	push_arm

	export LDFLAGS="$LDFLAGS -L$LIBS_PATH"
	export LDSHARED="$LIBLINK"

	# fake try to be able to cythonize generated files
	$HOSTPYTHON setup.py build_ext
	try find . -iname '*.pyx' -exec $CYTHON {} \;
	try $HOSTPYTHON setup.py build_ext -v
	try find build/lib.* -name "*.o" -exec $STRIP {} \;
	try $HOSTPYTHON setup.py install -O2
	try cp -a jnius/src/org $JAVACLASS_PATH

	unset LDSHARED
	pop_arm
}

function postbuild_pdpyjnius() {
	true
}
