from distutils.core import setup
import os

setup(name='android',
      version='1.0',
      packages=['android'],
      package_dir={'android': 'android'}
)
