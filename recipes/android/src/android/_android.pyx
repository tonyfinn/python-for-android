# Android-specific python services.
from jnius import autoclass

# Build info.
cdef extern char* BUILD_MANUFACTURER
cdef extern char* BUILD_MODEL
cdef extern char* BUILD_PRODUCT
cdef extern char* BUILD_VERSION_RELEASE

cdef extern void android_get_buildinfo()

class BuildInfo:
    MANUFACTURER = None
    MODEL = None
    PRODUCT = None
    VERSION_RELEASE = None

def get_buildinfo():
    android_get_buildinfo()
    binfo = BuildInfo()
    binfo.MANUFACTURER = BUILD_MANUFACTURER
    binfo.MODEL = BUILD_MODEL
    binfo.PRODUCT = BUILD_PRODUCT
    binfo.VERSION_RELEASE = BUILD_VERSION_RELEASE
    return binfo

cdef extern void android_start_service(char *, char *, char *)
def start_service(title=None, description=None, arg=None):
    cdef char *j_title = NULL
    cdef char *j_description = NULL
    if title is not None:
        j_title = <bytes>title
    if description is not None:
        j_description = <bytes>description
    if arg is not None:
        j_arg = <bytes>arg
    android_start_service(j_title, j_description, j_arg)

cdef extern void android_stop_service()
def stop_service():
    android_stop_service()

class AndroidService(object):
    '''Android service class.
    Run ``service/main.py`` from application directory as a service.

    :Parameters:
        `title`: str, default to 'Python service'
            Notification title.

        `description`: str, default to 'Kivy Python service started'
            Notification text.
    '''

    def __init__(self, title='Python service',
                 description='Kivy Python service started'):
        self.title = title
        self.description = description

    def start(self, arg=''):
        '''Start the service.

        :Parameters:
            `arg`: str, default to ''
                Argument to pass to a service,
                through environment variable ``PYTHON_SERVICE_ARGUMENT``.
        '''
        start_service(self.title, self.description, arg)

    def stop(self):
        '''Stop the service.
        '''
        stop_service()
