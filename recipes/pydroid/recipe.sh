#!/bin/bash

# version of your package
VERSION_pydroid=${VERSION_pydroid:-0.1.0}

# dependencies of this recipe
DEPS_pydroid=(android pdpyjnius)

# url of the package
URL_pydroid=http://jenkins.tonyfinn.com/builds/pydroid-$(echo $VERSION_pydroid).tar.gz

# md5 of the package
MD5_pydroid=

# default build path
BUILD_pydroid=$BUILD_PATH/pydroid/$(get_directory $URL_pydroid)

# default recipe path
RECIPE_pydroid=$RECIPES_PATH/pydroid

# function called for preparing source code if needed
# (you can apply patch etc here.)
function prebuild_pydroid() {
	true
}

# function called to build the source code
function build_pydroid() {
    if [ -z "$LOCAL_PYDROID_DIR" ]; then
        echo "Using downloaded PyDroid"
        cd $BUILD_pydroid
    else
        echo "Using local PyDroid"
        cd $LOCAL_PYDROID_DIR
    fi
    push_arm
    try $HOSTPYTHON setup.py install
    pop_arm
}

# function called after all the compile have been done
function postbuild_pydroid() {
	true
}
