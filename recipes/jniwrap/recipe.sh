#!/bin/bash

VERSION_jniwrap=1.0
URL_jniwrap=
MD5_jniwrap=
DEPS_jniwrap=(python)
BUILD_jniwrap=$BUILD_PATH/jniwrap/jniwrap-$VERSION_jniwrap
RECIPE_jniwrap=$RECIPES_PATH/jniwrap

function prebuild_jniwrap() {
	true
}

function shouldbuild_jniwrap() {
	if [ -f "$LIBS_PATH/libjniwrap.so" ]; then
		DO_BUILD=0
	fi
}

function build_jniwrap() {
	cd $SRC_PATH/jni

	push_arm
	try ndk-build V=1
	pop_arm

	try cp -a $SRC_PATH/libs/$ARCH/*.so $LIBS_PATH
}

function postbuild_jniwrap() {
	true
}
