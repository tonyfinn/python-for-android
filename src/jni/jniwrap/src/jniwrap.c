#include "jniwrapperstuff.h"
#include <jni.h>

static JNIEnv* JavaEnv = 0;

JNIEnv* PYDROID_GetJNIEnv() {
    return JavaEnv;
}

void JAVA_EXPORT_NAME1(PythonRunner_nativeSetupJNI, org_renpy_android) (JNIEnv* env, jobject thiz) {
    JavaEnv = env;
}
