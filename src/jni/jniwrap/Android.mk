LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)

LOCAL_MODULE := jniwrap

#Change C++ file extension as appropriate
LOCAL_CPP_EXTENSION := .cpp

LOCAL_SRC_FILES := $(LOCAL_PATH)/src/jniwrap.c

include $(BUILD_SHARED_LIBRARY)
