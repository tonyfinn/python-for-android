package com.tonyfinn.pydroid;

import android.app.Application;
import android.util.Log;
import org.renpy.android.PythonRunner;

public class PythonApplication  extends Application {
    private PythonRunner runner;
    private static PythonApplication application;
    private PyDroidApp app;

    public void onCreate() {
        long startTime = System.currentTimeMillis();
        super.onCreate();
        application = this;
        runner = new PythonRunner(this);
        new Thread(runner).start();
        synchronized (this) {
            while(app == null) {
                try {
                    this.wait(250);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
        long endTime = System.currentTimeMillis();
        Log.d("python", String.format("Python startup time: %s seconds", (endTime - startTime) / 1000));
    }

    public static PythonApplication getInstance() {
        return application;
    }

    public PyDroidApp getPyDroid() {
        return app;
    }

    public void set_pydroid_app(PyDroidApp app) {
        synchronized (this) {
            this.app = app;
            this.notify();
        }
    }

}
