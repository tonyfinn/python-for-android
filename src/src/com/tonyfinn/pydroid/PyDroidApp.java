package com.tonyfinn.pydroid;

import android.content.Intent;

/**
 * Created by tony on 12/11/14.
 */
public interface PyDroidApp {
    public PythonActivity getCurrentActivity();
    public PythonActivity launchActivity(ActivityProxy source, String name, Intent intent);
    public PythonActivity launchDefaultActivity(ActivityProxy source, Intent intent);
}
