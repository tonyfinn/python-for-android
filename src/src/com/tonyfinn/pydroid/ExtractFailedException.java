package com.tonyfinn.pydroid;

/**
 * Created by tony on 02/12/14.
 */
public class ExtractFailedException extends Exception {
    private String message;
    public ExtractFailedException(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }
}
