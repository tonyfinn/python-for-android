package com.tonyfinn.pydroid;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import java.util.Map;


public class ActivityProxy extends Activity {
    public static ActivityProxy mActivity = null;
    private PythonActivity pyAct;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mActivity = this;

        PythonApplication application = PythonApplication.getInstance();
        PyDroidApp pdApp = application.getPyDroid();
        Intent intent = getIntent();
        String targetActivity = intent.getStringExtra("pyActivity");
        if (targetActivity == null) {
            pyAct = pdApp.launchDefaultActivity(this, intent);
        } else {
            pyAct = pdApp.launchActivity(this, targetActivity, intent);
        }
        pyAct.on_create(savedInstanceState);
    }

    @Override
    public void onPause() {
        pyAct.on_pause();
        super.onPause();
    }
    
    @Override
    public void onResume() {
        super.onResume();
        pyAct.on_resume();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return pyAct.on_create_options_menu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return pyAct.on_options_item_selected(item);
    }

    public void launchOwnActivity(String activityName, Map<String, String> extras) {
        Intent intent = new Intent(this, ActivityProxy.class);
        intent.putExtra("pyActivity", activityName);
        for(String extra: extras.keySet()) {
            intent.putExtra(extra, extras.get(extra));
        }
        startActivity(intent);
    }

    @Override
    protected void onActivityResult(int requestCode, int responseCode, Intent data) {
        pyAct.on_activity_result(requestCode, responseCode, data);
    }

}

