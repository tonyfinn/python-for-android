package com.tonyfinn.pydroid;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

/**
 * Created by tony on 12/11/14.
 */
public interface PythonActivity {
    public void on_create(Bundle savedInstanceState);
    public void on_pause();
    public void on_resume();
    public boolean on_create_options_menu(Menu menu);
    public boolean on_options_item_selected(MenuItem item);
    public void on_activity_result(int requestCode, int responseCode, Intent data);
}
