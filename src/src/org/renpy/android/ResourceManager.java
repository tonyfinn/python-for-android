/**
 * This class takes care of managing resources for us. In our code, we
 * can't use R, since the name of the package containing R will
 * change. (This same code is used in both org.renpy.android and
 * org.renpy.pygame.) So this is the next best thing.
 */

package org.renpy.android;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.view.View;

public class ResourceManager {

    private Context ctx;
    private Resources res;

    public ResourceManager(Context context) {
        ctx = context;
        res = ctx.getResources();
    }

    public int getIdentifier(String name, String kind) {
        return res.getIdentifier(name, kind, ctx.getPackageName());
    }

    public String getString(String name) {

        try {
            return res.getString(getIdentifier(name, "string"));
        } catch (Exception e) {
            return null;
        }
    }


}
